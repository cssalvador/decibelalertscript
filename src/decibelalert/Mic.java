/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decibelalert;

import java.sql.Connection;
import java.sql.DriverManager;
import static java.sql.DriverManager.getConnection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.FloatControl;
import javax.sound.sampled.TargetDataLine;

/**
 *
 * @author Christian
 */
public class Mic extends Thread {

    boolean flag;
    TargetDataLine mic;
    byte[] buffer;
    AudioFormat format;

    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        Mic a = new Mic();
        host h = new host();
        connect();
        h.pruebaHost();

    }

    @Override
    public void run() {
        flag = true;
        startMic();
        while (flag) {
            try {
                send();
            } catch (SQLException ex) {
                Logger.getLogger(Mic.class.getName()).log(Level.SEVERE, null, ex);
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
                Logger.getLogger(Mic.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void send() throws SQLException {
        int id = 0;
        int sumaTodos = 0;
        String data = LocalDateTime.now().toString();
        String url = "jdbc:mysql://localhost:3306/BBDDdecibelAlert";
        String user = "userAdmin";
        String pass = "userAdmin";
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Connection con = getConnection(url, user, pass);

        PreparedStatement sentencia;
        sentencia = con.prepareStatement("INSERT INTO CAPTURASONIDO VALUES (?,?,?)");

        try {

            mic.read(buffer, 0, 512);

            for (int i = 0; i < buffer.length; i++) {

                sumaTodos += Integer.signum(buffer[i]) * buffer[i];

            }
            sumaTodos = sumaTodos / buffer.length;
            System.out.println("Media de sonido. " + sumaTodos);
            System.out.println("Hora actual. " + data);

        } catch (Exception ex) {

        }
        sentencia.setInt(1, id);
        sentencia.setString(2, data);
        sentencia.setInt(3, sumaTodos);
        sentencia.executeUpdate();
        

    }

    public void startMic() {
        try {
            format = new AudioFormat(8000, 8, 1, true, true); //Perque funcioni al ordena cambia els bits de 8 a 16
            DataLine.Info info = new DataLine.Info(TargetDataLine.class, format);
            mic = (TargetDataLine) AudioSystem.getLine(info);
            mic.open();
            /*FloatControl volumeControl = (FloatControl) mic.getControl(FloatControl.Type.PAN);
            volumeControl.setValue(volumeControl.getMaximum());*/
            mic.start();
            /* FloatControl volumeControl = (FloatControl) mic.getControl(FloatControl.Type.VOLUME);
            volumeControl.setValue(volumeControl.getMaximum());*/
            buffer = new byte[512];

        } catch (Exception exx) {
            System.out.println(exx.getMessage());
        }

    }

    public static void connect() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
        String url = "jdbc:mysql://localhost:3306/BBDDdecibelAlert?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
        String user = "userAdmin";
        String pass = "userAdmin";
        System.out.println("Conectando a Base de ...");
        try (Connection con = DriverManager.getConnection(url, user, pass)) {
            System.out.println("Conectado!!");
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }

}
