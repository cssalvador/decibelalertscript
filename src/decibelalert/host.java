/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package decibelalert;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class host {

    /*public static void main(String srgs[]) {
    }*/
    public void pruebaHost() {
        Mic a = new Mic();
        int count = 0;

        //hard code to use port 8080
        try (ServerSocket serverSocket = new ServerSocket(8080)) {
            serverSocket.getLocalPort();
            //System.out.println("I'm waiting here: " + serverSocket.getLocalPort());

            while (true) {

                try {
                    Socket socket = serverSocket.accept();

                    //count++;
                    a.start();
                    /* System.out.println("#" + count + " from "
                            + socket.getInetAddress() + ":"
                            + socket.getPort());*/

 /*  move to background thread
                    OutputStream outputStream = socket.getOutputStream();
                    try (PrintStream printStream = new PrintStream(outputStream)) {
                        printStream.print("Hello from Raspberry Pi, you are #" + count);
                    }
                     */
                    HostThread myHostThread = new HostThread(socket, count);
                    myHostThread.start();

                } catch (IOException ex) {
                    System.out.println(ex.toString());
                }
            }
        } catch (IOException ex) {
            System.out.println(ex.toString());
        }
    }

    public static class HostThread extends Thread {

        private Socket hostThreadSocket;
        int cnt;

        HostThread(Socket socket, int c) {
            hostThreadSocket = socket;
            cnt = c;
        }

        @Override
        public void run() {
            Mic a = new Mic();
            OutputStream outputStream;
            try {
                outputStream = hostThreadSocket.getOutputStream();

                try (PrintStream printStream = new PrintStream(outputStream)) {
                    //printStream.print(Arrays.toString(escribeContenido("/home/pi/file1.txt")));
                //    printStream.print(muestraContenido("/home/pi/file1.txt"));
                }
            } catch (IOException ex) {
                Logger.getLogger(host.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                try {
                    hostThreadSocket.close();
                } catch (IOException ex) {
                    Logger.getLogger(host.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        public static String muestraContenido(String archivo) throws FileNotFoundException, IOException {
            String cadena;
            String cc = "";
            FileReader f = new FileReader(archivo);
            //FileWriter fw = new FileWriter(archivo);
            BufferedReader b = new BufferedReader(f);
            // BufferedWriter w = new BufferedWriter(fw);
            while ((cadena = b.readLine()) != null) {
                System.out.println(cadena);

                cc = cc +"\n"+ cadena;
            }
            b.close();
            /* w.write("");
            w.close();*/
            return cc;
        }

        public static String[] escribeContenido(String archivo) throws FileNotFoundException, IOException {
            String[] lineas = {"Uno", "Dos", "Tres", "cuatro"};

            /**
             * FORMA 1 DE ESCRITURA *
             */
            FileWriter fichero = null;
            try {

                fichero = new FileWriter(archivo);

                // Escribimos linea a linea en el fichero
                for (String linea : lineas) {
                    fichero.write(linea + "\n");
                }

                fichero.close();

            } catch (Exception ex) {
                System.out.println("Mensaje de la excepción: " + ex.getMessage());
            }
            return lineas;
        }

    }
}
